import { Strategy as BnetStrategy } from 'passport-bnet';

const BNET_ID = '8a795513ffca448cbc4d45d1d321a497';
const BNET_SECRET = 'UoDVy0QYA9VnoA5MOJfB646qCf5PYLYB';
 
// Use the BnetStrategy within Passport.
const myBnetStrategy = new BnetStrategy({
    clientID: BNET_ID,
    clientSecret: BNET_SECRET,
    callbackURL: "https://localhost:8080/",
    region: "eu"
}, function(accessToken, refreshToken, profile, done) {
    return done(null, profile);
});

export default myBnetStrategy;