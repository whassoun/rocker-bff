import { Strategy as LocalStrategy } from 'passport-local';

const myLocalStrategy = new LocalStrategy((username, password, done) => {
    return done(null, false,  {'inte': 'inloggad'});
});

export default myLocalStrategy;