import { RESTDataSource, RequestOptions} from 'apollo-datasource-rest';

class CoinBaseAPI extends RESTDataSource 
{   
    constructor() 
    {
        super();
        this.baseURL = 'https://rest.coinapi.io/v1';
    }

    /**
     * Intercepting request and setting 
     * api security token to auth headers.
     * 
     * @param request 
     */
    willSendRequest(request: RequestOptions) 
    {
        /**
         * In a real project, i would have put my token in a .env file and keept the file git-ignored.
         */
        request.headers.set('X-CoinAPI-Key', '8EE91B24-0CC8-451A-8B66-4089CA5C64E2');
    }

    /**
     * @param periodId Optional with default value.
     * @param timeStart Optional with default value.
     * @param symbolId Optional with default value.
     * @returns Data with historical coin data from a data and period.
     */
    async getHistoricalCoinData(periodId: string = '1DAY', timeStart: string = '2021-04-01', assetId: string = 'BTC') 
    {
        return this.get(`/ohlcv/${assetId}/USD/history`, {
            period_id: periodId,
            time_start: timeStart
        });
    }

    /**
     * @returns Data with all exchanges and their symbol_id. 
     */
    async getSymbols() {
        return await this.get('/symbols');
    }

    /**
     * 
     * @returns Assets data.
     */
    async getAssets(assetId: string = '?') {
        return this.get(`/assets/${assetId}`);
    }

    /**
     * 
     * @returns Assets data.
     */
     async getAssetIcons() {
        return this.get('/assets/icons/32');
    }

}

export default CoinBaseAPI;