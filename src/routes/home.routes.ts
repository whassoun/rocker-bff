import { Router } from "express";
import passport from 'passport';

const homeRoutes = Router();
const basePath = '/';

homeRoutes.get(basePath, (req, res) => { res.json({'hello':'hello world'}) });
homeRoutes.get('/login', passport.authenticate('local', { failureRedirect: '/error' }), (req, res) => { res.redirect('/') });
homeRoutes.get('/bnet', passport.authenticate('bnet', { failureRedirect: '/error' }), (req, res) => { res.redirect('/') });

export default homeRoutes;