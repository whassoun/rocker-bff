import { gql } from 'apollo-server';
import CoinBaseAPI from './coin-base.api';
import {} from './battle-net.api';
import { dateScalar } from './scalar-types';

const typeDefs = gql`
    scalar Date
    type Query {
        historicalCoinData(periodId: String, timeStart: String, assetId: String): [CoinData],
        symbols: [Symbol],
        assets(asssetId: String): [Asset],
        assetIcons: [AssetIcon]
    }
    type Symbol {
        symbol_id: String,
        exchange_id: String
    }
    type AllAssetsConnection {
        asset: [Asset]
    }
    type Asset {
        asset_id: String,
        name: String,
    }
    type AssetIcon {
        asset_id: String,
        url: String,
    }
    type CoinData {
        time_period_start: Date,
        time_period_end: String,
        time_open: String,
        time_close: String,
        price_open: Float,
        price_high: Float,
        price_low: Float,
        price_close: Float,
        volume_traded: Float,
        trades_count: Float
    }
`;

const resolvers = {
    Date: dateScalar,
    Query: {
        historicalCoinData: async (_, { periodId, timeStart, assetId }, { dataSources }) => {
            return dataSources.CoinBaseAPI.getHistoricalCoinData(periodId, timeStart, assetId);
        },
        symbols: async (_, __, { dataSources }) => {
            return dataSources.CoinBaseAPI.getSymbols();
        },
        assets: async (_, { assetId }, { dataSources }) => {
            return dataSources.CoinBaseAPI.getAssets(assetId);
        },
        assetIcons: async (_, __, { dataSources }) => {
            return dataSources.CoinBaseAPI.getAssetIcons();
        }, 
    }
};

const dataSources = () => {
    return {
        CoinBaseAPI: new CoinBaseAPI(),
    };
}

export {
    typeDefs,
    resolvers,
    dataSources
}