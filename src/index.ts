import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { typeDefs, resolvers, dataSources } from './schema';
import routes from './routes/';
import passport from 'passport';
import myLocalStrategy from './auth/local.strategy';
import myBnetStrategy from './auth/bnet.strategy';

// Define apollo server.
const apolloServer = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources
});

// Start apollo server.
apolloServer.start();

// Define express app.
const app = express();

// Apply express app to apollo server.
apolloServer.applyMiddleware({ app });

// passport.serializeUser(function(user, done) { //In serialize user you decide what to store in the session. Here I'm storing the user id only.
//     done(null, user.id);
// });

// passport.deserializeUser(function(id, done) { //Here you retrieve all the info of the user from the session storage using the user id stored in the session earlier using serialize user.
//     //
// });

// Add passport auth strategies.
passport.use('local', myLocalStrategy);
passport.use('bnet', myBnetStrategy);

// Init passport for auth.
app.use(passport.initialize());

// Add routes.
app.use(routes);

// Run app.
app.listen({ port: 8080 });