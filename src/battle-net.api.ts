import { RESTDataSource, RequestOptions} from 'apollo-datasource-rest';

class BattleNetAPI extends RESTDataSource 
{   
    constructor() 
    {
        super();
        this.baseURL = 'https://rest.coinapi.io/v1';
    }

    /**
     * Intercepting request and setting 
     * api security token to auth headers.
     * 
     * @param request 
     */
    willSendRequest(request: RequestOptions) 
    {
        /**
         * In a real project, i would have put my token in a .env file and keept the file git-ignored.
         */
        request.headers.set('X-CoinAPI-Key', '8EE91B24-0CC8-451A-8B66-4089CA5C64E2');
    }
}

export default BattleNetAPI;